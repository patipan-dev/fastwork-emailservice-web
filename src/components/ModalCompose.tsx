import { useEffect, useState } from "react"
import Button from "@material-ui/core/Button"
import TextField from "@material-ui/core/TextField"
import Dialog from "@material-ui/core/Dialog"
import DialogActions from "@material-ui/core/DialogActions"
import DialogContent from "@material-ui/core/DialogContent"
import DialogTitle from "@material-ui/core/DialogTitle"
import { sendMail } from "src/services/sendMail"
import { connect } from "react-redux"
import { Box, CircularProgress, Grid } from "@material-ui/core"
import SendIcon from "@material-ui/icons/Send"
import {
  ModalComposeActionTypes,
  SentLogsActionTypes,
  ToastActionTypes,
} from "src/models/dto/actionType"
import styled from "@emotion/styled"

const ModalCompose = ({ dispatch, state }: any) => {
  const [recipients, setRecipients] = useState("")
  const [subject, setSubject] = useState("")
  const [message, setMessage] = useState("")

  const [isLoading, setIsLoading] = useState(false)

  const isFormDataReady =
    recipients.length > 0 && subject.length > 0 && message.length > 0

  const WrapperCircularProgressStyled = styled(Box)`
    display: flex;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  `

  const handleModalComposeClose = () => {
    dispatch({
      type: ModalComposeActionTypes.MODAL_COMPOSE_CLOSE,
    })
  }

  const handleSendMail = async () => {
    setIsLoading(true)
    const { token } = state?.currentUser
    try {
      const response = await sendMail({ recipients, subject, message }, token)
      dispatch({
        type: SentLogsActionTypes.SENT_LOGS_ADD_DATA,
        payload: {
          data: response.data.data,
        },
      })
      dispatch({
        type: ToastActionTypes.TOAST_OPEN,
        payload: {
          open: true,
          title: `${response.data.data.status}`,
          message: `Message sent by ${response.data.data.provider}`,
          severity: "success",
        },
      })
      handleModalComposeClose()
      setIsLoading(false)
    } catch (error) {
      if (error.response.status !== 500) {
        dispatch({
          type: SentLogsActionTypes.SENT_LOGS_ADD_DATA,
          payload: {
            data: error.response.data.data,
          },
        })
      }
      dispatch({
        type: ToastActionTypes.TOAST_OPEN,
        payload: {
          open: true,
          title: error.code,
          message: error.message,
          severity: "error",
        },
      })
      setIsLoading(false)
    }
  }

  useEffect(() => {
    setRecipients(state.modalCompose.recipients)
    setSubject(state.modalCompose.subject)
    setMessage(state.modalCompose.message)
  }, [state.modalCompose])

  return (
    <>
      <Dialog
        open={state.modalCompose.open}
        onClose={handleModalComposeClose}
        aria-labelledby="form-dialog-title"
        fullWidth
      >
        <DialogTitle id="form-dialog-title">{state.modalCompose.title}</DialogTitle>
        <DialogContent>
          <Grid container spacing={4} direction="column">
            <Grid item>
              <TextField
                value={recipients}
                autoFocus
                margin="dense"
                id="email"
                label="Recipients"
                type="email"
                variant="filled"
                fullWidth
                onChange={(event) => setRecipients(event.target.value)}
              />
            </Grid>
            <Grid item>
              <TextField
                value={subject}
                id="subject"
                label="Subject"
                variant="filled"
                fullWidth
                onChange={(event) => setSubject(event.target.value)}
              />
            </Grid>
            <Grid item>
              <TextField
                value={message}
                id="message"
                label="Message"
                variant="filled"
                fullWidth
                multiline
                rows={4}
                onChange={(event) => setMessage(event.target.value)}
              />
            </Grid>
          </Grid>
        </DialogContent>
        <Box py={1}>
          <DialogActions>
            <Button onClick={handleModalComposeClose}>Cancel</Button>
            <Box position="relative">
              <Button
                onClick={handleSendMail}
                variant="contained"
                color="primary"
                endIcon={<SendIcon />}
                disabled={isLoading || !isFormDataReady}
              >
                Send
              </Button>
              {isLoading && (
                <WrapperCircularProgressStyled>
                  <CircularProgress size={20} />
                </WrapperCircularProgressStyled>
              )}
            </Box>
          </DialogActions>
        </Box>
      </Dialog>
    </>
  )
}

const mapStateToProps = (state: any) => ({
  state: {
    currentUser: state.currentUser,
    modalCompose: state.modalCompose,
    sentLogs: state.sentLogs,
  },
})

export default connect(mapStateToProps)(ModalCompose)
