import React, { useState } from "react"
import AppBar from "@material-ui/core/AppBar"
import Button from "@material-ui/core/Button"
import Grid from "@material-ui/core/Grid"
import Toolbar from "@material-ui/core/Toolbar"
import Typography from "@material-ui/core/Typography"
import { Badge, Box, CircularProgress } from "@material-ui/core"
import { useRouter } from "next/router"
import { signOut } from "src/services/auth"
import { connect } from "react-redux"
import ExitToAppIcon from "@material-ui/icons/ExitToApp"
import EmailIcon from "@material-ui/icons/Email"
import { ToastActionTypes } from "src/models/dto/actionType"
import styled from "@emotion/styled"

const Header = ({ state, dispatch }: any) => {
  const [isHandleSignOutLoading, setIsHandleSignOutLoading] = useState(false)

  const router = useRouter()

  const WrapperCircularProgressStyled = styled(Box)`
    display: flex;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  `

  const handleSignOut = async () => {
    setIsHandleSignOutLoading(true)
    try {
      await signOut()
      setIsHandleSignOutLoading(false)
      return router.push("/signin")
    } catch (error) {
      dispatch({
        type: ToastActionTypes.TOAST_OPEN,
        payload: {
          open: true,
          title: error.code,
          message: error.message,
          severity: "error",
        },
      })
      setIsHandleSignOutLoading(false)
    }
  }

  return (
    <>
      <AppBar color="primary" position="sticky" elevation={0}>
        <Toolbar>
          <Box flex={1}>
            <Grid container justify="space-between">
              <Box display="flex" alignItems="center">
                <Box mr={2}>
                  <Badge
                    badgeContent={state.sentLogs?.data?.length}
                    color="secondary"
                  >
                    <EmailIcon fontSize="large" />
                  </Badge>
                </Box>
                <Typography color="inherit" variant="h5" component="h1">
                  Email Service
                </Typography>
              </Box>
              <Box py={2} ml="auto" display="flex" alignItems="center">
                <Box>
                  <Typography
                    color="inherit"
                    variant="h6"
                    component="div"
                    align="right"
                  >
                    {state.currentUser?.displayName}
                  </Typography>
                  <Typography color="inherit" component="span" variant="subtitle2">
                    {state.currentUser?.email}
                  </Typography>
                </Box>
                <Box ml={2}>
                  <Box position="relative">
                    <Button
                      variant="outlined"
                      color="inherit"
                      size="small"
                      onClick={handleSignOut}
                      endIcon={<ExitToAppIcon />}
                      disabled={isHandleSignOutLoading}
                    >
                      Sign Out
                    </Button>
                    {isHandleSignOutLoading && (
                      <WrapperCircularProgressStyled>
                        <CircularProgress size={20} />
                      </WrapperCircularProgressStyled>
                    )}
                  </Box>
                </Box>
              </Box>
            </Grid>
          </Box>
        </Toolbar>
      </AppBar>
    </>
  )
}

const mapStateToProps = (state: any) => ({
  state: {
    currentUser: state.currentUser,
    sentLogs: state.sentLogs,
  },
})

export default connect(mapStateToProps)(Header)
