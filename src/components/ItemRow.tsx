import {
  Box,
  CircularProgress,
  IconButton,
  TableCell,
  TableRow,
  Tooltip,
  useTheme,
} from "@material-ui/core"
import CheckCircleIcon from "@material-ui/icons/CheckCircle"
import ErrorIcon from "@material-ui/icons/Error"
import RefreshIcon from "@material-ui/icons/Refresh"
import { useState } from "react"
import { connect } from "react-redux"
import {
  ModalComposeActionTypes,
  SentLogsActionTypes,
  ToastActionTypes,
} from "src/models/dto/actionType"
import { sendMail } from "src/services/sendMail"
import EditIcon from "@material-ui/icons/Edit"
import styled from "@emotion/styled"

const ItemRow = ({ dispatch, state, data, index }: any) => {
  const [isLoading, setIsLoading] = useState(false)

  const theme = useTheme()

  const CheckCircleIconStyled = styled(CheckCircleIcon)`
    color: ${theme.palette.success.main};
  `

  const TableRowStyled = styled(TableRow)`
    background-color: ${index % 2 === 0 ? "transparent" : "rgba(255,255,255,0.1)"};
  `

  const WrapperCircularProgressStyled = styled(Box)`
    display: flex;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  `

  const TableCellProviderStyled = styled(TableCell)`
    text-transform: capitalize;
  `

  const formatDate = (timestamp: number) => {
    if (timestamp) {
      const options = {
        year: "numeric",
        month: "short",
        day: "numeric",
        hour: "2-digit",
        minute: "2-digit",
        second: "2-digit",
        hour12: false,
      }
      const date = new Date(timestamp)
      return date.toLocaleDateString("en-US", options)
    } else {
      return "-"
    }
  }

  const handleModalComposeEditMessageOpen = () => {
    dispatch({
      type: ModalComposeActionTypes.MODAL_COMPOSE_EDIT_MESSAGE_OPEN,
      payload: {
        recipients: data.recipients,
        subject: data.subject,
        message: data.message,
      },
    })
  }

  const handleSendMail = async () => {
    setIsLoading(true)
    const { token } = state?.currentUser
    try {
      const response = await sendMail(
        {
          recipients: data.recipients,
          subject: data.subject,
          message: data.message,
        },
        token
      )
      dispatch({
        type: SentLogsActionTypes.SENT_LOGS_ADD_DATA,
        payload: {
          data: response.data.data,
        },
      })
      dispatch({
        type: ToastActionTypes.TOAST_OPEN,
        payload: {
          open: true,
          title: `${response.data.data.status}`,
          message: `Message sent by ${response.data.data.provider}`,
          severity: "success",
        },
      })
      setIsLoading(false)
    } catch (error) {
      if (error.response.status !== 500) {
        dispatch({
          type: SentLogsActionTypes.SENT_LOGS_ADD_DATA,
          payload: {
            data: error.response.data.data,
          },
        })
      }
      dispatch({
        type: ToastActionTypes.TOAST_OPEN,
        payload: {
          open: true,
          title: error.response.data.code,
          message: error.response.data.message,
          severity: "error",
        },
      })
      setIsLoading(false)
    }
  }

  const IconStatus = (status: string) => {
    switch (status) {
      case "SUCCESS":
        return (
          <Tooltip title={status}>
            <CheckCircleIconStyled />
          </Tooltip>
        )
      case "FAILED":
        return (
          <>
            <Tooltip title={status}>
              <ErrorIcon color="error" />
            </Tooltip>
            <Tooltip title="Resend">
              <Box position="relative">
                <IconButton onClick={handleSendMail} disabled={isLoading}>
                  <RefreshIcon color="inherit" />
                </IconButton>
                {isLoading && (
                  <WrapperCircularProgressStyled>
                    <CircularProgress size={24} />
                  </WrapperCircularProgressStyled>
                )}
              </Box>
            </Tooltip>
          </>
        )
      default:
        return null
    }
  }

  return (
    <TableRowStyled>
      <TableCell component="th" scope="row">
        {data.recipients}
      </TableCell>
      <TableCell>{data.subject}</TableCell>
      <TableCell>{data.message}</TableCell>
      <TableCellProviderStyled>{data.provider}</TableCellProviderStyled>
      <TableCell align="center">{formatDate(data.sentAt)}</TableCell>
      <TableCell align="center">{IconStatus(data.status)}</TableCell>
      <TableCell align="center">
        <Tooltip title="Edit">
          <IconButton onClick={handleModalComposeEditMessageOpen}>
            <EditIcon color="inherit" fontSize="small" />
          </IconButton>
        </Tooltip>
      </TableCell>
    </TableRowStyled>
  )
}

const mapStateToProps = (state: any) => ({
  state: { currentUser: state.currentUser, sentLogs: state.sentLogs },
})

export default connect(mapStateToProps)(ItemRow)
