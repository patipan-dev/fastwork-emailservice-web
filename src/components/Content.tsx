import AppBar from "@material-ui/core/AppBar"
import Toolbar from "@material-ui/core/Toolbar"
import Paper from "@material-ui/core/Paper"
import Grid from "@material-ui/core/Grid"
import Tooltip from "@material-ui/core/Tooltip"
import IconButton from "@material-ui/core/IconButton"
import RefreshIcon from "@material-ui/icons/Refresh"
import {
  Backdrop,
  Box,
  Checkbox,
  CircularProgress,
  FormControlLabel,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@material-ui/core"
import ItemRow from "./ItemRow"
import { useEffect, useState } from "react"
import { getHistories } from "src/services/history"
import { getMailProviders, updateMailProvider } from "src/services/mailProvider"
import { connect } from "react-redux"
import { SentLogsActionTypes, ToastActionTypes } from "src/models/dto/actionType"
import styled from "@emotion/styled"

const Content = ({ dispatch, state }: any) => {
  const [isSentLogLoading, setIsSentLogLoading] = useState(true)
  const [isMailProviderLoading, setIsMailProviderLoading] = useState(true)

  const [isProvidersEnabled, setIsProvidersEnabled] = useState([])

  const WrapperContentStyled = styled(Box)`
    position: relative;
    min-height: 50vh;
  `

  const TableContainerStyled = styled(TableContainer)`
    max-height: calc(100vh - 230px);
  `

  const BackdropStyled = styled(Backdrop)`
    position: absolute !important;
    z-index: 10 !important;
  `

  const FormControlLabelStyled = styled(FormControlLabel)`
    .MuiFormControlLabel-label {
      text-transform: capitalize;
    }
  `

  const handleGetMailProvider = async () => {
    setIsMailProviderLoading(true)
    try {
      const response = await getMailProviders(state.currentUser?.token)
      setIsProvidersEnabled(response.data.data)
      setIsMailProviderLoading(false)
    } catch (error) {
      dispatch({
        type: ToastActionTypes.TOAST_OPEN,
        payload: {
          open: true,
          title: error.response.data.code,
          message: error.response.data.message,
          severity: "error",
        },
      })
      setIsMailProviderLoading(false)
    }
  }

  const handleEnablesMailProvider = async (id: string, enable: boolean) => {
    setIsMailProviderLoading(true)
    try {
      await updateMailProvider({ id, enable }, state.currentUser?.token)
      handleGetMailProvider()
      setIsMailProviderLoading(false)
    } catch (error) {
      dispatch({
        type: ToastActionTypes.TOAST_OPEN,
        payload: {
          open: true,
          title: error.response.data.code,
          message: error.response.data.message,
          severity: "error",
        },
      })
      setIsMailProviderLoading(false)
    }
  }

  const handleGetHistories = async () => {
    setIsSentLogLoading(true)
    try {
      const response = await getHistories(
        { offset: 0, limit: 40 },
        state.currentUser?.token
      )
      dispatch({
        type: SentLogsActionTypes.SENT_LOGS_SET_DATA,
        payload: {
          data: response.data.data,
        },
      })
      setIsSentLogLoading(false)
    } catch (error) {
      dispatch({
        type: ToastActionTypes.TOAST_OPEN,
        payload: {
          open: true,
          title: error.response.data.code,
          message: error.response.data.message,
          severity: "error",
        },
      })
      setIsSentLogLoading(false)
    }
  }

  useEffect(() => {
    if (state.currentUser?.token) {
      handleGetMailProvider()
      handleGetHistories()
    }
  }, [state.currentUser?.token])

  return (
    <>
      <Paper elevation={5}>
        <Box px={3} display="flex" alignItems="center">
          <Typography>Enable Provider :</Typography>
          <Box ml={2}>
            {isProvidersEnabled.map((item: any, index: number) => (
              <FormControlLabelStyled
                key={index}
                label={item.providerName}
                control={
                  <Checkbox
                    checked={item.enable}
                    name={item.providerName}
                    onChange={() => {
                      handleEnablesMailProvider(item.id, !item.enable)
                    }}
                  />
                }
              />
            ))}
          </Box>
          {isMailProviderLoading && (
            <Box py={1}>
              <CircularProgress size={20} />
            </Box>
          )}
        </Box>
        <AppBar position="static" color="default" elevation={0}>
          <Toolbar>
            <Grid container justify="space-between" alignItems="center">
              <Grid item>
                <Typography variant="h6" component="h6">
                  Sent Log
                </Typography>
              </Grid>
              <Grid item>
                <Tooltip title="Reload">
                  <IconButton onClick={handleGetHistories}>
                    <RefreshIcon color="inherit" />
                  </IconButton>
                </Tooltip>
              </Grid>
            </Grid>
          </Toolbar>
        </AppBar>
        <WrapperContentStyled>
          {state.sentLogs?.data?.length > 0 && (
            <Box pb={1}>
              <TableContainerStyled>
                <Table stickyHeader aria-label="sticky table">
                  <TableHead>
                    <TableRow>
                      <TableCell>Recipients</TableCell>
                      <TableCell>Subject</TableCell>
                      <TableCell>Message</TableCell>
                      <TableCell>Provider</TableCell>
                      <TableCell align="center" width={120}>
                        Send At
                      </TableCell>
                      <TableCell align="center">Status</TableCell>
                      <TableCell />
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {state.sentLogs.data?.map((item: any, index: number) => (
                      <ItemRow key={index} index={index} data={item} />
                    ))}
                  </TableBody>
                </Table>
              </TableContainerStyled>
            </Box>
          )}
          {state.sentLogs?.data?.length === 0 && !isSentLogLoading && (
            <Box p={6}>
              <Typography color="textSecondary" align="center">
                No Result
              </Typography>
            </Box>
          )}
          <BackdropStyled open={isSentLogLoading}>
            <CircularProgress color="inherit" />
          </BackdropStyled>
        </WrapperContentStyled>
      </Paper>
    </>
  )
}

const mapStateToProps = (state: any) => ({
  state: { currentUser: state.currentUser, sentLogs: state.sentLogs },
})

export default connect(mapStateToProps)(Content)
