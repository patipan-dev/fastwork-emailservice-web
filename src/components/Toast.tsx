import { Snackbar } from "@material-ui/core"
import { Alert, AlertTitle } from "@material-ui/lab"
import { connect } from "react-redux"
import { ToastActionTypes } from "src/models/dto/actionType"

const Toast = ({ dispatch, state }: any) => {
  const handleCloseToast = () =>
    dispatch({
      type: ToastActionTypes.TOAST_CLOSE,
    })
  return (
    <Snackbar
      open={state.toast.open}
      autoHideDuration={state.toast.severity === "error" ? null : 6000}
      onClose={handleCloseToast}
    >
      <Alert
        variant="filled"
        severity={state.toast.severity}
        onClose={handleCloseToast}
      >
        <AlertTitle>{state.toast.title}</AlertTitle>
        {state.toast.message}
      </Alert>
    </Snackbar>
  )
}

const mapStateToProps = (state: any) => ({
  state: { toast: state.toast },
})

export default connect(mapStateToProps)(Toast)
