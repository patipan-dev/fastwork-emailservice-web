import Axios from "axios"
import getConfig from "next/config"
import { GetHistories } from "src/models/dto/form"

const { publicRuntimeConfig } = getConfig()
const { API_DOMAIN } = publicRuntimeConfig

export const getHistories = async (params: GetHistories, token: string) => {
  return await Axios.get(
    `${API_DOMAIN}/email?offset=${params.offset}&limit=${params.limit}`,
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  )
}
