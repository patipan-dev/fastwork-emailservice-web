import Axios from "axios"
import getConfig from "next/config"
import { UpdateMailProvider } from "src/models/dto/form"

const { publicRuntimeConfig } = getConfig()
const { API_DOMAIN } = publicRuntimeConfig

export const getMailProviders = async (token: string) => {
  return await Axios.get(`${API_DOMAIN}/email/provider`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  })
}

export const updateMailProvider = async (
  data: UpdateMailProvider,
  token: string
) => {
  return await Axios.put(`${API_DOMAIN}/email/provider`, data, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  })
}
