import Axios from "axios"
import { SendMail } from "src/models/dto/form"
import getConfig from "next/config"

const { publicRuntimeConfig } = getConfig()
const { API_DOMAIN } = publicRuntimeConfig

export const sendMail = async (data: SendMail, token: string) => {
  return await Axios.post(`${API_DOMAIN}/email/send`, data, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  })
}
