import Axios from "axios"
import { SignUp, SignIn } from "src/models/dto/form"
import firebase from "src/connect/firebase"
import getConfig from "next/config"

const { publicRuntimeConfig } = getConfig()
const { API_DOMAIN } = publicRuntimeConfig

export const signIn = (data: SignIn) => {
  return firebase.auth().signInWithEmailAndPassword(data.email, data.password)
}

export const signUp = async (data: SignUp) => {
  return await Axios.post(`${API_DOMAIN}/user/signup`, data)
}

export const signOut = () => {
  return firebase.auth().signOut()
}

export const getIdToken = async () => {
  return await firebase.auth().currentUser?.getIdToken()
}
