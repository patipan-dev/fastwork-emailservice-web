import * as firebase from "firebase/app"
import "firebase/auth"
import getConfig from "next/config"

const { publicRuntimeConfig } = getConfig()
const {
  FIREBASE_API_KEY,
  FIREBASE_AUTH_DOMAIN,
  FIREBASE_DATABASE_URL,
  FIREBASE_PROJECT_ID,
  FIREBASE_STORAGE_BUCKET,
  FIREBASE_MESSAGING_SENDER_ID,
  FIREBASE_APP_ID,
} = publicRuntimeConfig

const config = {
  apiKey: FIREBASE_API_KEY,
  authDomain: FIREBASE_AUTH_DOMAIN,
  databaseURL: FIREBASE_DATABASE_URL,
  projectId: FIREBASE_PROJECT_ID,
  storageBucket: FIREBASE_STORAGE_BUCKET,
  messagingSenderId: FIREBASE_MESSAGING_SENDER_ID,
  appId: FIREBASE_APP_ID,
}

if (!firebase.apps.length) {
  try {
    firebase.initializeApp(config)
  } catch (error) {
    console.log("firebase.initializeApp error", error)
  }
}

export const auth = firebase.auth

export default firebase
