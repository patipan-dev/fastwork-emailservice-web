import Avatar from "@material-ui/core/Avatar"
import Button from "@material-ui/core/Button"
import TextField from "@material-ui/core/TextField"
import Link from "@material-ui/core/Link"
import LockOutlinedIcon from "@material-ui/icons/LockOutlined"
import Typography from "@material-ui/core/Typography"
import Container from "@material-ui/core/Container"
import {
  Box,
  FormControl,
  Grid,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
  useTheme,
} from "@material-ui/core"
import { getIdToken, signIn } from "src/services/auth"
import { useEffect, useState } from "react"
import { connect } from "react-redux"
import { CurrentUserActionTypes, ToastActionTypes } from "src/models/dto/actionType"
import { useRouter } from "next/router"
import CircularProgress from "@material-ui/core/CircularProgress"
import { Visibility, VisibilityOff } from "@material-ui/icons"
import styled from "@emotion/styled"

const SignInPage = ({ state, dispatch }: any) => {
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const [isPasswordShow, setIsPasswordShow] = useState(false)

  const [isLoading, setIsLoading] = useState(false)

  const isFormDataReady = email.length > 0 && password.length > 5

  const theme = useTheme()
  const router = useRouter()

  const WrapperIconSignInStyled = styled(Avatar)`
    background-color: ${theme.palette.primary.main} !important;
  `

  const WrapperCircularProgressStyled = styled(Box)`
    display: flex;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  `

  const handleSignIn = async () => {
    setIsLoading(true)
    try {
      const response = await signIn({ email, password })
      const token = await getIdToken()
      dispatch({
        type: CurrentUserActionTypes.CURRENT_USER_SET_DATA,
        payload: {
          token: token,
          displayName: response?.user?.displayName,
          email: response?.user?.email,
          uid: response?.user?.uid,
        },
      })
      router.push("/")
      setIsLoading(false)
    } catch (error) {
      dispatch({
        type: ToastActionTypes.TOAST_OPEN,
        payload: {
          open: true,
          title: error.code,
          message: error.message,
          severity: "error",
        },
      })
      setPassword("")
      setIsLoading(false)
    }
  }

  useEffect(() => {
    if (state.currentUser?.token) {
      router.push("/")
    }
  }, [state.currentUser?.token])

  return (
    <Container component="main" maxWidth="xs">
      <Box mt={6} display="flex" flexDirection="column" alignItems="center">
        <WrapperIconSignInStyled>
          <LockOutlinedIcon />
        </WrapperIconSignInStyled>
        <Box mt={2}>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
        </Box>
        <Box mt={4}>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
            onChange={(event) => setEmail(event.target.value)}
          />
          <FormControl margin="normal" required fullWidth variant="outlined">
            <InputLabel htmlFor="password">Password</InputLabel>
            <OutlinedInput
              id="password"
              name="password"
              value={password}
              autoComplete="current-password"
              type={isPasswordShow ? "text" : "password"}
              labelWidth={84}
              onChange={(event) => setPassword(event.target.value)}
              onKeyPress={(e) => {
                if (e.key === "Enter") {
                  !isLoading && isFormDataReady && handleSignIn()
                }
              }}
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={() => setIsPasswordShow(!isPasswordShow)}
                    edge="end"
                  >
                    {isPasswordShow ? <Visibility /> : <VisibilityOff />}
                  </IconButton>
                </InputAdornment>
              }
            />
          </FormControl>
          <Box mt={3}>
            <Box position="relative">
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                size="large"
                onClick={handleSignIn}
                disabled={isLoading || !isFormDataReady}
              >
                Sign In
              </Button>
              {isLoading && (
                <WrapperCircularProgressStyled>
                  <CircularProgress size={24} />
                </WrapperCircularProgressStyled>
              )}
            </Box>
          </Box>
          <Box mt={3}>
            <Grid container justify="flex-end">
              <Grid item>
                <Link href="/signup" variant="body2">
                  {"Don't have an account? Sign Up"}
                </Link>
              </Grid>
            </Grid>
          </Box>
        </Box>
      </Box>
    </Container>
  )
}

const mapStateToProps = (state: any) => ({
  state: { currentUser: state.currentUser },
})

export default connect(mapStateToProps)(SignInPage)
