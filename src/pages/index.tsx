import { Box, Container, Fab, Tooltip } from "@material-ui/core"
import CssBaseline from "@material-ui/core/CssBaseline"
import { connect } from "react-redux"
import Content from "src/components/Content"
import Header from "src/components/Header"
import AddIcon from "@material-ui/icons/Add"
import { ModalComposeActionTypes } from "src/models/dto/actionType"
import { useEffect } from "react"
import { useRouter } from "next/router"

const HomePage = ({ state, dispatch }: any) => {
  const router = useRouter()

  const handleModalComposeNewMessageOpen = () => {
    dispatch({
      type: ModalComposeActionTypes.MODAL_COMPOSE_NEW_MESSAGE_OPEN,
    })
  }

  useEffect(() => {
    if (!state.currentUser?.token) {
      router.push("/signin")
    }
  }, [state.currentUser?.token])

  return (
    <>
      <CssBaseline />
      <Header />
      <Box py={5}>
        <Container>
          <Content />
        </Container>
      </Box>
      <Box position="fixed" right={40} bottom={40}>
        <Tooltip title="Compose">
          <Fab
            color="primary"
            aria-label="add"
            onClick={handleModalComposeNewMessageOpen}
          >
            <AddIcon fontSize="large" />
          </Fab>
        </Tooltip>
      </Box>
    </>
  )
}

const mapStateToProps = (state: any) => ({
  state: { currentUser: state.currentUser },
})

export default connect(mapStateToProps)(HomePage)
