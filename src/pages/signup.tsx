import React, { useEffect, useState } from "react"
import Avatar from "@material-ui/core/Avatar"
import Button from "@material-ui/core/Button"
import CssBaseline from "@material-ui/core/CssBaseline"
import TextField from "@material-ui/core/TextField"
import Link from "@material-ui/core/Link"
import Grid from "@material-ui/core/Grid"
import LockOutlinedIcon from "@material-ui/icons/LockOutlined"
import Typography from "@material-ui/core/Typography"
import Container from "@material-ui/core/Container"
import {
  Box,
  CircularProgress,
  FormControl,
  FormHelperText,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
  useTheme,
} from "@material-ui/core"
import { getIdToken, signIn, signUp } from "src/services/auth"
import { CurrentUserActionTypes, ToastActionTypes } from "src/models/dto/actionType"
import { useRouter } from "next/router"
import { connect } from "react-redux"
import { Visibility, VisibilityOff } from "@material-ui/icons"
import styled from "@emotion/styled"

const SignUpPage = ({ state, dispatch }: any) => {
  const [displayName, setDisplayName] = useState("")
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const [isPasswordShow, setIsPasswordShow] = useState(false)

  const isFormDataReady =
    displayName.length > 3 && email.length > 0 && password.length > 5

  const [isLoading, setIsLoading] = useState(false)

  const theme = useTheme()
  const router = useRouter()

  const WrapperIconSignInStyled = styled(Avatar)`
    background-color: ${theme.palette.primary.main} !important;
  `

  const WrapperCircularProgressStyled = styled(Box)`
    display: flex;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  `

  const handleSignUp = async () => {
    setIsLoading(true)
    try {
      await signUp({ displayName, email, password })
      handleSignIn(email, password)
    } catch (error) {
      dispatch({
        type: ToastActionTypes.TOAST_OPEN,
        payload: {
          open: true,
          title: error.code,
          message: error.message,
          severity: "error",
        },
      })
      setIsLoading(false)
    }
  }

  const handleSignIn = async (email: string, password: string) => {
    try {
      const response = await signIn({ email, password })
      const token = await getIdToken()
      dispatch({
        type: CurrentUserActionTypes.CURRENT_USER_SET_DATA,
        payload: {
          token: token,
          displayName: response?.user?.displayName,
          email: response?.user?.email,
          uid: response?.user?.uid,
        },
      })
      router.push("/")
      setIsLoading(false)
    } catch (error) {
      dispatch({
        type: ToastActionTypes.TOAST_OPEN,
        payload: {
          open: true,
          title: error.code,
          message: error.message,
          severity: "error",
        },
      })
      setIsLoading(false)
    }
  }

  useEffect(() => {
    if (state.currentUser?.token) {
      router.push("/")
    }
  }, [state.currentUser?.token])

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <Box mt={6} display="flex" flexDirection="column" alignItems="center">
        <WrapperIconSignInStyled>
          <LockOutlinedIcon />
        </WrapperIconSignInStyled>
        <Box mt={2}>
          <Typography component="h1" variant="h5">
            Sign up
          </Typography>
        </Box>
        <Box mt={4}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                autoFocus
                variant="outlined"
                required
                fullWidth
                id="displayname"
                label="Display Name"
                name="displayname"
                autoComplete="displayname"
                onChange={(event) => setDisplayName(event.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                onChange={(event) => setEmail(event.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <FormControl required fullWidth variant="outlined">
                <InputLabel htmlFor="password">Password</InputLabel>
                <OutlinedInput
                  id="password"
                  name="password"
                  value={password}
                  autoComplete="new-password"
                  type={isPasswordShow ? "text" : "password"}
                  labelWidth={84}
                  onChange={(event) => setPassword(event.target.value)}
                  onKeyPress={(e) => {
                    if (e.key === "Enter") {
                      !isLoading && isFormDataReady && handleSignUp()
                    }
                  }}
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={() => setIsPasswordShow(!isPasswordShow)}
                        edge="end"
                      >
                        {isPasswordShow ? <Visibility /> : <VisibilityOff />}
                      </IconButton>
                    </InputAdornment>
                  }
                />
              </FormControl>
              <FormHelperText>
                * minimum password length is 6 characters
              </FormHelperText>
            </Grid>
          </Grid>
          <Box mt={2}>
            <Grid container justify="flex-end">
              <Grid item>
                <Link href="/signin" variant="body2">
                  Already have an account? Sign in
                </Link>
              </Grid>
            </Grid>
          </Box>
          <Box mt={2}>
            <Box position="relative">
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                size="large"
                onClick={handleSignUp}
                disabled={isLoading || !isFormDataReady}
              >
                Sign Up
              </Button>
              {isLoading && (
                <WrapperCircularProgressStyled>
                  <CircularProgress size={24} />
                </WrapperCircularProgressStyled>
              )}
            </Box>
          </Box>
        </Box>
      </Box>
    </Container>
  )
}

const mapStateToProps = (state: any) => ({
  state: { currentUser: state.currentUser },
})

export default connect(mapStateToProps)(SignUpPage)
