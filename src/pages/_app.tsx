import { useEffect, useState } from "react"
import { Provider } from "react-redux"
import { AppProps } from "next/app"
import Head from "next/head"
import {
  Backdrop,
  CircularProgress,
  CssBaseline,
  ThemeProvider,
} from "@material-ui/core"
import { getIdToken } from "src/services/auth"
import firebase from "src/connect/firebase"
import store from "src/store"
import theme from "src/assets/theme"
import Toast from "src/components/Toast"
import ModalCompose from "src/components/ModalCompose"
import styled from "@emotion/styled"

import "src/assets/css/reset.css"

const MyApp = ({ Component, pageProps }: AppProps) => {
  const [user, setUser] = useState<any>({
    token: "",
    displayName: "",
    email: "",
    uid: "",
  })
  const [isLoading, setIsLoading] = useState(true)

  const BackdropStyled = styled(Backdrop)`
    position: absolute !important;
    z-index: 9999 !important;
  `

  useEffect(() => {
    firebase.auth().onAuthStateChanged(async (userData) => {
      if (userData) {
        const { email, uid, displayName } = userData
        const token = await getIdToken()
        setUser({
          email: email,
          uid: uid,
          displayName: displayName,
          token: token,
        })
        setIsLoading(false)
      } else {
        setUser({
          email: "",
          uid: "",
          displayName: "",
          token: "",
        })
        setIsLoading(false)
      }
    })
  }, [])

  useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles: any = document.querySelector("#jss-server-side")
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles)
    }
  }, [])

  return (
    <>
      <Head>
        <title>Email Service</title>
        <meta
          name="viewport"
          content="minimum-scale=1, initial-scale=1, width=device-width"
        />
      </Head>
      {isLoading ? (
        <BackdropStyled open={true}>
          <CircularProgress color="primary" />
        </BackdropStyled>
      ) : (
        <Provider store={store({ currentUser: user })}>
          <ThemeProvider theme={theme}>
            <CssBaseline />
            <Component {...pageProps} />
            <Toast />
            <ModalCompose />
          </ThemeProvider>
        </Provider>
      )}
    </>
  )
}

export default MyApp
