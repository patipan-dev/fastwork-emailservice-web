import { createStore, applyMiddleware, combineReducers } from "redux"
import { composeWithDevTools } from "redux-devtools-extension"
import logger from "redux-logger"
import thunk from "redux-thunk"
import currentUser from "./currentUser"
import toast from "./toast"
import modalCompose from "./modalCompose"
import sentLogs from "./sentLogs"

const store = (initialData: any) => {
  let middleware
  if (process.env.NODE_ENV == "development") {
    middleware = applyMiddleware(thunk, logger)
  } else {
    middleware = applyMiddleware(thunk)
  }

  const userInitialData = () => currentUser(initialData.currentUser, null)

  const reducers = combineReducers({
    currentUser: userInitialData,
    toast,
    modalCompose,
    sentLogs,
  })

  return createStore(reducers, composeWithDevTools(middleware))
}

export default store
