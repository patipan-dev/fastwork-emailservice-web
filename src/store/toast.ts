import { ToastActionTypes } from "src/models/dto/actionType"

const INITIAL_STATE = {
  open: false,
  title: "",
  message: "",
  severity: "",
}

const toast = (state: any = INITIAL_STATE, action: any) => {
  switch (action?.type) {
    case ToastActionTypes.TOAST_OPEN:
      return {
        ...state,
        ...action.payload,
      }
    case ToastActionTypes.TOAST_CLOSE:
      return {
        ...state,
        open: false,
      }
    default:
      return state
  }
}

export default toast
