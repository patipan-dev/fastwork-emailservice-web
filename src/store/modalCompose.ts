import { ModalComposeActionTypes } from "src/models/dto/actionType"

const INITIAL_STATE = {
  open: false,
  title: "",
  recipients: "",
  subject: "",
  message: "",
}

const modalCompose = (state: any = INITIAL_STATE, action: any) => {
  switch (action?.type) {
    case ModalComposeActionTypes.MODAL_COMPOSE_NEW_MESSAGE_OPEN:
      return {
        ...state,
        ...INITIAL_STATE,
        open: true,
        title: "New Message",
      }
    case ModalComposeActionTypes.MODAL_COMPOSE_EDIT_MESSAGE_OPEN:
      return {
        ...state,
        open: true,
        title: "Edit Message",
        ...action.payload,
      }
    case ModalComposeActionTypes.MODAL_COMPOSE_CLOSE:
      return {
        ...state,
        open: false,
      }
    default:
      return state
  }
}

export default modalCompose
