import { CurrentUserActionTypes } from "src/models/dto/actionType"

const INITIAL_STATE = {
  token: "",
  displayName: "",
  email: "",
  uid: "",
}

const currentUser = (state = INITIAL_STATE, action: any) => {
  switch (action?.type) {
    case CurrentUserActionTypes.CURRENT_USER_SET_DATA:
      return {
        ...state,
        ...action.payload,
      }
    default:
      return state
  }
}

export default currentUser
