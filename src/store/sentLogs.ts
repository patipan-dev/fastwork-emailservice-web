import { SentLogsActionTypes } from "src/models/dto/actionType"

const INITIAL_STATE = {
  data: [],
  length: 0,
}

const sentLogs = (state: any = INITIAL_STATE, action: any) => {
  switch (action?.type) {
    case SentLogsActionTypes.SENT_LOGS_SET_DATA:
      const mapData = action.payload.data.map((i: any) => ({
        recipients: i.recipients,
        subject: i.subject,
        message: i.message,
        provider: i.provider,
        sentAt: i.createdAt,
        status: i.status,
      }))
      return {
        ...state,
        data: [...mapData],
        length: action.payload.data.length,
      }
    case SentLogsActionTypes.SENT_LOGS_ADD_DATA:
      const data = {
        recipients: action.payload.data.recipients,
        subject: action.payload.data.subject,
        message: action.payload.data.message,
        provider: action.payload.data.provider,
        sentAt: action.payload.data.createdAt,
        status: action.payload.data.status,
      }
      return {
        ...state,
        data: [data, ...state.data],
        length: state.data.length + 1,
      }
    default:
      return state
  }
}

export default sentLogs
