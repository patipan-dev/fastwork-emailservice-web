import { CurrentUserActionTypes } from "./actionType"

export class ActionUser {
  type!: CurrentUserActionTypes
  payload!: {
    token: string
    displayName: string
    email: string
    uid: string
  }
}
