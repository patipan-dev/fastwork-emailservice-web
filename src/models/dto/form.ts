export class SignUp {
  email!: string
  password!: string
  displayName!: string
}

export class SignIn {
  email!: string
  password!: string
}

export class SendMail {
  recipients!: string
  subject!: string
  message!: string
}

export class UpdateMailProvider {
  id!: string
  enable!: boolean
}

export class GetHistories {
  offset!: number
  limit!: number
}
